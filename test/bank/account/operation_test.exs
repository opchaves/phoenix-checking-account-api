defmodule Bank.Account.OperationTest do
  use Bank.DataCase

  alias Bank.Account.Operation

  @valid_attrs %{account_number: 123, operation: "deposit", description: "A deposit", amount: 1000.0, date: ~N[2017-08-01 10:00:00.000000]}
  @invalid_amount %{@valid_attrs | amount: -150.0}
  @invalid_operation %{@valid_attrs | operation: "invalid"}
  @invalid_attrs %{account_number: nil, amount: nil, date: nil, description: nil, operation: nil}

  test "changeset with valid attributes" do
    changeset = Operation.changeset(%Operation{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Operation.changeset(%Operation{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "changeset with invalid amount" do
    changeset = Operation.changeset(%Operation{}, @invalid_amount)
    refute changeset.valid?
  end

  test "changeset with invalid operation" do
    changeset = Operation.changeset(%Operation{}, @invalid_operation)
    refute changeset.valid?
  end
end
